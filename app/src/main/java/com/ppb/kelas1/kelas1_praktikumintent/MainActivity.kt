package com.ppb.kelas1.kelas1_praktikumintent

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.ppb.kelas1.kelas1_praktikumintent.model.Mahasiswa

class MainActivity : AppCompatActivity() {
    lateinit var btnMove: Button
    lateinit var btnMoveWithData: Button
    lateinit var btnMoveWithObject: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //deklarasi variabel
        btnMove = findViewById(R.id.btn_move)
        btnMoveWithData = findViewById(R.id.btn_move_with_data)
        btnMoveWithObject = findViewById(R.id.btn_move_with_object)
        actionButtonMove()
        actionButtonMoveWithData()
        actionButtonMoveWithObject()
    }

    fun actionButtonMoveWithObject() {
        val mahasiswa = Mahasiswa(
            "2242101234",
            "Epson Simangunsong",
            20,
            true
        )
        btnMoveWithObject.setOnClickListener {
            intent = Intent(this, MoveWithObjectActivity::class.java)
            intent.putExtra("mahasiswa",mahasiswa)
            startActivity(intent)
        }
    }

    fun actionButtonMoveWithData() {
        btnMoveWithData.setOnClickListener {
            intent = Intent(this, MoveWithDataActivity::class.java)
            intent.putExtra("name", "Snowman")
            intent.putExtra("age", 10)
            startActivity(intent)
        }
    }

    fun actionButtonMove() {
        btnMove.setOnClickListener {
            intent = Intent(this, MoveActivity::class.java)
            startActivity(intent)
        }
    }
}