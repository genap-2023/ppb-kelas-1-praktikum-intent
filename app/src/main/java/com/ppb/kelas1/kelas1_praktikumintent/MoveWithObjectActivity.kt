package com.ppb.kelas1.kelas1_praktikumintent

import android.os.Build
import android.os.Build.VERSION
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.ppb.kelas1.kelas1_praktikumintent.model.Mahasiswa

class MoveWithObjectActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_move_with_object)
        val mahasiswa = if (VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.getParcelableExtra("mahasiswa", Mahasiswa::class.java)
        } else {
            intent.getParcelableExtra<Mahasiswa>("mahasiswa")
        }
        val tvNim = findViewById<TextView>(R.id.tv_nim)
        val tvNama = findViewById<TextView>(R.id.tv_nama)
        val tvUmur = findViewById<TextView>(R.id.tv_umur)
        val tvLulus = findViewById<TextView>(R.id.tv_lulus)

        tvNim.text = mahasiswa?.nim
        tvNama.text = mahasiswa?.nama
        tvUmur.text = mahasiswa?.umur.toString()
        tvLulus.text = if (mahasiswa!!.isLulus) {
            "Sudah Lulus"
        } else {
            "Belum Lulus"
        }

    }
}