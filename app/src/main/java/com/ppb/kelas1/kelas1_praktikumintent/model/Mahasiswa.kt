package com.ppb.kelas1.kelas1_praktikumintent.model

import android.os.Parcel
import android.os.Parcelable

data class Mahasiswa(
    val nim: String?,
    val nama: String?,
    val umur: Int,
    val isLulus: Boolean
):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(nim)
        parcel.writeString(nama)
        parcel.writeInt(umur)
        parcel.writeByte(if (isLulus) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Mahasiswa> {
        override fun createFromParcel(parcel: Parcel): Mahasiswa {
            return Mahasiswa(parcel)
        }

        override fun newArray(size: Int): Array<Mahasiswa?> {
            return arrayOfNulls(size)
        }
    }
}
