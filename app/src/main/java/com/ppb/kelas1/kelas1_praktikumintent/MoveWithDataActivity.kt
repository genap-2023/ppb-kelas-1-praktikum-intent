package com.ppb.kelas1.kelas1_praktikumintent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MoveWithDataActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_move_with_data)
        val tvName = findViewById<TextView>(R.id.tv_name)
        val tvAge = findViewById<TextView>(R.id.tv_age)
        //ambil data dari intent
        val name = intent.getStringExtra("name")
        val age = intent.getIntExtra("age", 0)
        //set ke view
        tvName.text = name.toString()
        tvAge.text = age.toString()
    }
}